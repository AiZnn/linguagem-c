#include <stdio.h>
#include <stdlib.h>

int main() {
  
  int ano; 
  
  do {
    system("cls");
    printf("Digite um o ano:");
    scanf("%d", &ano);
  } while (ano <= 0);
  if (ano % 4 == 0)
      printf("O ano %d e bissexto.", ano);
  else
      printf("O ano %d nao e bissexto .", ano);
  return 0:

}