#include<stdio.h>

int fatorial, numero;

int main()
{
  printf("Digite um numero para ver seu fatorial:\n");
  scanf("%d", &numero) ;

  for(fatorial = 1; numero > 1; numero--)
  {
      fatorial = fatorial * numero;
  }

  printf("\nFatorial: %d", fatorial);
  return 0;
}