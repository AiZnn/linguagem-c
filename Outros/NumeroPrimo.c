#include <stdio.h>
#include <stdlib.h>

int main() {
  
  int contador, numero; 
  int resultado = 0;
  
  do {
    system("clear");
    printf("Digite um numero inteiro e positivo: ");
    scanf("%d", &numero);
  } while (numero <= 0);
  
  for (contador = 1; contador <= numero; contador++) {
    if (numero % 2 == 0) { 
     resultado++;
    }
  }
  
  if (resultado == 2)
    printf("O numero %d e primo.", numero);
  else
    printf("O numero %d nao e primo.", numero);
  return 0;
}